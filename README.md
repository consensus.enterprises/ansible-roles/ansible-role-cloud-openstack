Consensus: Cloud OpenStack [![pipeline status](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-cloud-openstack/badges/master/pipeline.svg)](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-cloud-openstack/commits/master)
==========================

A brief description of the role goes here.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Download OpenStack Credentials
------------------------------

These instructions are specific to [CloudA](https://www.clouda.ca). Modify as needed.

1. Login to the [Infrastructure Dashboard](https://dash.ca-bc-1.clouda.ca).
1. Visit the [API Access](https://dash.ca-bc-1.clouda.ca/project/api_access/) page.
1. Click the "Download CLI Authentication File" link.
1. Move the file to the project directory and rename it: `mv ~/Downloads/operations\@consensus.enterprises-openrc.sh ./clouda-openrc.sh`
    1. *N.B.* this file is different for different clouda regions; handling this TBD/TODO.
    1. *If needed*: Edit the file and replace `OS_USERNAME` with your own (i.e. `<name>@consensus.enterprises`).


Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
