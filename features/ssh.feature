@ssh @wip
Feature: SSH testing
  In order to test OpenStack provisioning
  As a DevOps engineer
  I need to ensure I can connect to remote hosts and execute commands

  Scenario: Check that I can SSH to a known host
     When I run "ssh aegir0 echo 'Hello, World!'"
     Then I should get:
       """ 
       Hello, World!
       """ 
