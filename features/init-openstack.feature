Feature: Initialize Openstack projects
  In order to use the consensus.cloud-openstack role
  As a DevOps engineer
  I need to be able to initialize a cloud-openstack project

  @init @openstack
  Scenario: Smoke test initializing cloud-openstack project
    Given I run "make -n init-role-openstack"
     Then I should get:
      """
      echo "Setting up OpenStack project (consensus.cloud-openstack)."
      echo "Finished setting up OpenStack project (consensus.cloud-openstack)."
      """

  @init @openstack
  Scenario: Test initializing cloud-openstack project
      Given I bootstrap this Ansible role
        And I run "make init-project-openstack"
       Then the following files should exist:
        """
        roles/consensus.admin-users
        roles/consensus.cloud-openstack
        inventory/host_vars/localhost.yml
        inventory/openstack.yml
        inventory/openstack_inventory.py
        """
       Then I run "make"
        And I should get:
        """
        facts
           Show all available cloud resources.
        flavors 
           Show all available VM flavors (sizes).
        images 
           Show all available OS images.
        infra
           Ensure all cloud resources exist and are provisioned/configured.
        """
       And the following files should exist:
       """
       inventory/group_vars/example_group.yml
       inventory/host_vars/example-host.yml
       playbooks/groups/example_group.yml
       playbooks/hosts/example-host.yml
       """

  @init @openstack
  Scenario: Test initializing cloud-openstack project with host and group
      Given I bootstrap this Ansible role
        And I run "make init-project-openstack host=myhost group=mygroup"
       Then the following files should exist:
        """
        inventory/host_vars/localhost.yml
        """
       Then the file "inventory/host_vars/localhost.yml" should contain:
        """
        - name: 'myhost'
        groups:
        - mygroup
        """
       And the following files should not exist:
       """
       inventory/group_vars/example_group.yml
       inventory/host_vars/example-host.yml
       playbooks/groups/example_group.yml
       playbooks/hosts/example-host.yml
       """
       And the following files should exist:
       """
       inventory/group_vars/mygroup.yml
       inventory/host_vars/myhost.yml
       playbooks/groups/mygroup.yml
       playbooks/hosts/myhost.yml
       """


