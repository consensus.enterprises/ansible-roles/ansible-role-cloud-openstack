if [[ "$OS_USERNAME" ]]; then
  echo "consensus.cloud-openstack is already bootstrapped."
  return
fi

echo "Bootstrapping consensus.cloud-openstack."

CWD=`pwd`
CLOUDA_OPENRC_SH="${CWD}/clouda-openrc.sh"


if [[ ! -f "${CLOUDA_OPENRC_SH}" ]]; then
  echo "WARNING: No CloudA credential file found at ${CLOUDA_OPENRC_SH}. Cloud infrastructure provisioning probably won't work until this is fixed. Please see https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-cloud-openstack#download-openstack-credentials"
  return
fi

if [[ -z "${OS_USERNAME}" ]]; then
  . ${CLOUDA_OPENRC_SH}
fi

alias uncloud-openstack='unset OS_USERNAME'
