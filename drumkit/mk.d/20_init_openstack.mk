OPENSTACK_HOST_VARS_TEMPLATE ?= $(CLOUD_OPENSTACK_ROLE_DIR)files/templates/localhost.yml.j2
OPENSTACK_SUBMODULES_CONFIG_FILE := $(CLOUD_OPENSTACK_ROLE_DIR)drumkit/submodules.openstack

.PHONY: init-role-openstack-intro init-role-openstack init-openstack-definition

# Initialize openstack_submodules hash
$(call initialize_submodules_hash,$(OPENSTACK_SUBMODULES_CONFIG_FILE),openstack_submodules)

openstack_submodule_paths=$(call keys,openstack_submodules)
$(openstack_submodule_paths):
	@echo Submoduling $@
	@git submodule add $(call get,openstack_submodules,$@) $@

init-role-openstack-intro:
	@echo "Setting up OpenStack project (consensus.cloud-openstack)."
init-role-openstack: init-role-openstack-intro init-project-ansible init-openstack-definition $(openstack_submodule_paths) $(ANSIBLE_INVENTORY_DIR)/openstack.yml $(ANSIBLE_INVENTORY_DIR)/openstack_inventory.py $(BOOTSTRAP_D)/10_openstack_credentials.sh
	@echo "Finished setting up OpenStack project (consensus.cloud-openstack)." 
	@echo "Next, download a cloud credentials script (see https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-openstack#download-openstack-credentials)."
	@echo "Then configure your cloud infrastructure in 'playbooks/host_vars/localhost.yml'."
	@echo "Finally, run 'make infra' to provision your new cloud infrastructure."

init-openstack-definition-intro:
	@echo Deploying openstack infrastructure manifest variables file.
init-openstack-definition: init-openstack-definition-intro
	@make -s ansible-add-host-vars-file ANSIBLE_HOST_VARS_TEMPLATE=$(OPENSTACK_HOST_VARS_TEMPLATE) ANSIBLE_HOST_VARS_FILENAME=localhost.yml host=$(host) group=$(group)
	@echo Finished deploying openstack infrastructure manifest variables file.

$(ANSIBLE_INVENTORY_DIR)/openstack.yml:
	@echo Deploying openstack inventory config file.
	@ln -s ../$(CLOUD_OPENSTACK_ROLE_DIR)inventory/openstack.yml $@

$(ANSIBLE_INVENTORY_DIR)/openstack_inventory.py:
	@echo Deploying openstack inventory script.
	@ln -s ../$(CLOUD_OPENSTACK_ROLE_DIR)inventory/openstack_inventory.py $@
	@echo Deploying custom ansible configuration file.
	@cp $(CLOUD_OPENSTACK_ROLE_DIR)/files/ansible.cfg .


$(BOOTSTRAP_D)/10_openstack_credentials.sh:
	@echo Deploying openstack credentials bootstrap file.
	@ln -s ../../$(CLOUD_OPENSTACK_ROLE_DIR)$@ $@

clean-role-openstack:
	@echo Cleaning up openstack inventory config file.
	@rm -f $(ANSIBLE_INVENTORY_DIR)/openstack.yml
	@echo Cleaning up openstack inventory script.
	@rm -f $(ANSIBLE_INVENTORY_DIR)/openstack_inventory.py
	@echo Cleaning up openstack infrastructure manifest variables file.
	@rm -f $(ANSIBLE_HOST_VARS_DIR)/localhost.yml
	@echo Cleaning up openstack credentials bootstrap file.
	@rm -f $(BOOTSTRAP_D)/10_openstack_credentials.sh
	@echo Deploying default Drumkit ansible configuration file.
	@cp $(FILES_DIR)/ansible/ansible.cfg $(PROJECT_ROOT)

# TODO: delete submodules, too
