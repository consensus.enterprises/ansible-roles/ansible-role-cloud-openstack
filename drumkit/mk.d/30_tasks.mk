# For use with Drumkit; see https://drumk.it

.PHONY: infra facts flavors images check-credentials

check-credentials:
ifndef OS_USERNAME
	$(error Drumkit: consensus.cloud-openstack is not bootstrapped. Try 'source d')
endif

facts: ansible-playbook check-credentials ## [tags=flavors,networks,ports,servers,subnets,images,keystone_domains] Show all available cloud resources.
	$(ANSIBLE_PLAYBOOK_CMD) $(CLOUD_OPENSTACK_ROLE_PLAYBOOKS_DIR)/facts.yml

flavors: ansible-playbook check-credentials ## Show all available VM flavors (sizes).
	$(ANSIBLE_PLAYBOOK_CMD) $(CLOUD_OPENSTACK_ROLE_PLAYBOOKS_DIR)/flavors.yml 

images: ansible-playbook check-credentials ## Show all available OS images.
	$(ANSIBLE_PLAYBOOK_CMD) $(CLOUD_OPENSTACK_ROLE_PLAYBOOKS_DIR)/images.yml

infra: ansible-playbook check-credentials $(ANSIBLE_HOST_VARS_DIR)/localhost.yml ## [tags=servers,security_groups,keypairs] Ensure all cloud resources exist and are provisioned/configured.
	$(ANSIBLE_PLAYBOOK_CMD) $(CLOUD_OPENSTACK_ROLE_PLAYBOOKS_DIR)/infra.yml
